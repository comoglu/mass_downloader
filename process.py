#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import sys
import os
from obspy.clients.fdsn.mass_downloader import CircularDomain, \
Restrictions, MassDownloader

from obspy.geodetics.base import kilometer2degrees

# local libraries
import station
import configuration
import logger
import plot

class Process:
    """
    The class used for defining station
    """
    def __init__(self):
        """
        Constructor of class
        """
        self.stations=[]
        self.config=None

        # Directory to save the entire processing and results
        self.save_dir=None
        # File path to log
        self.log=None

        ########################### inner variables ###########################

        self.mseeds_dir=None
        self.stations_dir=None
        self.plots_dir=None
        self.stations_map=None


    # populate stations list with station objects from station xml and
    # mini-seed
    def loadStations(self):
        # find all stations from stations directory
        _files=sorted(os.listdir(self.stations_dir))

        self.log.info('Loading stations (having 3 or 6 ' +
        'traces/waveforms)...')
        self.log.info('Reading inventory from: ' +
        self.stations_dir+'/ ...')
        self.log.info('Reading mini-seeds from: ' +
        self.mseeds_dir+'/ ...')

        # for each station
        for _file in _files:
            # creates a station's object
            if ".xml" in _file:
                sta=station.Station()
                # loads data, according to xml and mseeds
                # <path>/stations/NET.STATION.xml
                # <path>/mseeds/NET.STATION.*

                sta.load(self.stations_dir+'/'+_file,
                         self.mseeds_dir+'/'+_file[:-3]+'*')

                # append to station list, stations having 3 or 6 traces
                # (*Z, *N, *E -acc or vel-)
                if (sta.count()==3 or sta.count()==6):
                    self.stations.append(sta)
                    self.log.info(str(sta.network)+'.'+str(sta.code) + ' '
                    + str([str(_tr.stats.channel) for _tr in sta.stream.traces]))

    def loadConfig(self, xml):
        # load configuration xml to config object (to memory)
        self.config=configuration.Configuration()
        self.config.read(xml)

    def download(self):

        # set domain
        _minradius=kilometer2degrees(self.config.mindist)
        _maxradius=kilometer2degrees(self.config.maxdist)
        _domain = CircularDomain(latitude=self.config.centlat,
                             longitude=self.config.centlon,
                             minradius=_minradius,
                             maxradius=_maxradius)

        # set waveform restrictions
        _restrictions = Restrictions(
            starttime=self.config.mseed_starttime-2,
            endtime=self.config.mseed_endtime+2,
            network=self.config.network,
            reject_channels_with_gaps=self.config.reject_gaps,
            minimum_length=self.config.minimum_length,
            sanitize=self.config.sanitize,
            minimum_interstation_distance_in_m=\
                                            self.config.mindist_interstation,
            location_priorities=self.config.location,
            channel_priorities=self.config.channel
            )

        # init
        # fdsn services priority matters on same stations
        _fdsn = MassDownloader(providers=self.config.providers)

        # get current path (pwd)
        _cur_path=os.getcwd()

        # set path to download data and stations' xml
        os.chdir(self.save_dir)

        # set mseed and xml dirs
        _mseed_storage = (
        os.path.basename(os.path.dirname(self.mseeds_dir)) + '/' +
        os.path.basename(self.mseeds_dir) +\
        '/{network}.{station}.{channel}.{location}.{starttime}.{endtime}.mseed')

        _stationxml_storage = (os.path.basename(self.stations_dir) + \
        "/{network}.{station}.xml")

        # download content
        _fdsn.download(_domain, _restrictions,
                       mseed_storage=_mseed_storage,
                       stationxml_storage=_stationxml_storage)

        # set path to init path
        os.chdir(_cur_path)

        self._rename_mseeds()


    def init_dir(self, output_path):

        # save directory
        self.save_dir=output_path

        self.mseeds_dir=os.path.join(self.save_dir, 'mseeds/raw')
        self.plots_dir=os.path.join(self.save_dir, 'mseeds/raw/plots')
        self.stations_dir=os.path.join(self.save_dir, 'stations')
        # map of stations and reference point
        self.stations_map=os.path.join(self.stations_dir, 'stations_map.png')

        # set log file
        self.log=logger.Logger("obspy.clients.fdsn.mass_downloader", \
        self.save_dir+"/log")

        # if plots dir doesn't exist
        if not os.path.exists(self.plots_dir):
            os.makedirs(self.plots_dir)

        # if stations dir doesn't exist
        if not os.path.exists(self.stations_dir):
            os.makedirs(self.stations_dir)

    def _rename_mseeds(self):
        """
        Renaming mseeds to format NET.STA.CHA.mseed
        """
        _files=sorted(os.listdir(self.mseeds_dir))
        # for each station
        for _file in _files:
            # if it's a file
            if os.path.isfile(os.path.join(self.mseeds_dir, _file)):
                _temp=_file.split('.')
                _new_file = _temp[0] + '.' + _temp[1] + '.' + _temp[2] + \
                '.mseed'
                # renaming file
                os.rename(os.path.join(self.mseeds_dir, _file),
                          os.path.join(self.mseeds_dir, _new_file))

    def plot_mseeds(self):

        for station in self.stations:
            _png=station.network+'.'+ station.code + '.png'
            station.plot(os.path.join(self.plots_dir, _png))

    def correct_mseeds(self):

        for station in self.stations:

            # trim mseeds to specific timestamp
            station.trim(self.config.mseed_starttime,
                         self.config.mseed_endtime)

            # remove traces with gaps
            station.removeGaps()

            # remove traces with clipped data
            station.removeClips()

            # remove instrument response
            station.removeResponse(display="VEL")

            # rotate streams when necessary
            station.rotate()

        #remove streams with zero traces
        self.stations[:]=[_sta for _sta in self.stations if not _sta.count==0]

    def plot_map(self):

        plot.plot_map(self.stations, self.config.centlat, self.config.centlon,
        self.config.maxdist, self.stations_map)





if __name__ == "__main__":

    proc=Process()

    # loading configuration from xml file
    #proc.loadConfig(sys.argv[1])
    proc.loadConfig('configuration.xml')

    # print configuration to stdout
    proc.config.echo()

    # write configuration to log file
    proc.log.configuration(proc.config)


    # load stations
   # proc.loadStations()
