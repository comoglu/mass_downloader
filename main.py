#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True # maybe false!
import os

import configuration
import fdsn
import stream


if __name__ == "__main__":

    stations=[]

    # reading configuration xml
    config=configuration.Configuration()
    config.read('configuration.xml')

    # print configuration to log file
  #  config.log.configuration(config)

    fdsn.download(config)

   # config.log.info('Loading stations...')
    stream.loadStations(config,stations)

    stream.plotStations(config, stations)
   # stream.write2xml(config,stations)


    #config.log.info('Remove clipped streams...')
  #sstream.removeClipped(config, stations)

    #config.log.info('Plot stations...')
    #stream.plotStations(config, stations)

    #config.log.info('Remove instrument response...')
    stream.removeResponse(config,stations)

   # stream.plotStations(config, stations)

   # for station in stations:
    #   station.echo()

class Stations:
    """
    The class used for defining stations list
    """
    def __init__(self):
        """
        Constructor of class
        """
        self.list=[]
        self.config=None





# plot stations
def plotStations(config, stations):

    # create plot directory
    _plot=config.save_dir+"/plots/init/"
    if not os.path.exists(_plot):
        os.makedirs(_plot)

    for station in stations:

        # print station's info
        station.echo()

        # plot all station's traces/waveforms to one plot
        station.stream.plot(outfile=_plot+station.network+'.'+station.code+'.png')




# remove clipped streams
def removeClipped(config, stations, threshold=0.8):
    """
    Remove clipped data
    """

    _max_value = threshold * (2**23)

    # for each station
    for station in stations:

        # print station's info
        station.echo()

        # copy all streams
        _streams=station.stream.copy()

        # return traces not clipped
        station.stream.traces[:] = [_trace for _trace in station.stream.traces if _max_value > _trace.max()]

    # remove stations with no streams
    stations[:] = [_station for _station in stations if bool(len(_station.stream))]












# remove instrument response
def removeResponse(config, stations, pre_filt = [0.001, 0.005, 10, 20]):

    # create plot directory
    _plot=config.save_dir+"/mseeds/corrected/"
    if not os.path.exists(_plot):
        os.makedirs(_plot)

    # create plot directory
    _plot=config.save_dir+"/plots/corrected/"
    if not os.path.exists(_plot):
        os.makedirs(_plot)




    for station in stations:
 #Trace.remove_response(inventory=None, output='VEL', water_level=60, pre_filt=None, zero_mean=True, taper=True, taper_fraction=0.05, plot=False, fig=None, **kwargs)
        station.stream.remove_response(inventory=station.inv, pre_filt=pre_filt, output="VEL")

        for trace in station.stream.traces:
            trace.write(config.save_dir+"/mseeds/corrected/" + station.network+'.'+station.code+'.'+trace.stats.channel + ".mseed", format="MSEED")

       # plot all station's traces/waveforms to one plot
        station.stream.plot(outfile=_plot+station.network+'.'+station.code+'.png')


def write2xml(config,stations):

    _stations = etree.Element('stations')
    _root = etree.ElementTree(_stations)
    for station in stations:
        _station = etree.SubElement(_stations, 'station')
        _network = etree.SubElement(_station, 'network')
        _network.text=station.network
        _code = etree.SubElement(_station, 'code')
        _code.text=station.code
        _channels = etree.SubElement(_station, 'channels')
        _selected = etree.SubElement(_channels, 'selected')
        _selected.text=str(station.stream.select(component="N")[0].stats.channel)
        _available = etree.SubElement(_channels, 'available')
        _list=[]
        _list[:] = [str(_tr.stats.channel) for _tr in station.stream.traces]
        _available.text=str(_list)


    _file = open('stations.xml', 'w')
    _root.write(_file, pretty_print=True)