#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True
import logging
import os

class Logger():

    def __init__(self, service, file_dir):
         self.service = service
         self.file_dir = file_dir

         # create path to log file if not exists
         if not os.path.exists(os.path.dirname(self.file_dir)):
             os.makedirs(os.path.dirname(self.file_dir))

         self.logger = logging.getLogger(self.service)
         self.hdlr = logging.FileHandler(self.file_dir)
         self.form = \
              logging.Formatter("%(asctime)s %(levelname)s - %(message)s")
         self.hdlr.setFormatter(self.form)
         self.logger.addHandler(self.hdlr)
         self.logger.setLevel(logging.DEBUG)


    def info(self, msg):
        """
        writes an info message to log file
        """
        self.logger.info(msg+"\n")


    def exception(self):
        self.logger.exception("Exception Raised")


    def error(self, msg):
        self.logger.error(msg)


    def configuration(self, configuration):

        _message = "Configuration\'s parameters:\n"
        _set_d = vars(configuration)

        for key in _set_d:
            _message += str(key) + ": " + str(_set_d[key]) + "\n"

        self.info(_message)



