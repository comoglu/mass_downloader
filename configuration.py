#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True # maybe false!

#import datetime
#import codecs
import os
#import shutil
#import numpy as np
#import subprocess
#import decimal
#import operator

import logger
import xml.etree.cElementTree as ET
from obspy.core.utcdatetime import UTCDateTime

###############################################################################
# DEFINE CLASS "Configuration"                                                #
###############################################################################

class Configuration:
    """
    The class used for assigning configuration
    """
    def __init__(self):
        """
        Constructor of class
        """
        # List of FDSN client service URLS
        # The order in the list also determines their priority, if data is
        # available at more then one provider it will always be downloaded
        # from the provider that comes first in the list.
        self.providers=None
        # A circular domain defined by a center point and minimum and maximum
        # radius from that point in degrees.
        # However, user provides distance in kilometers that is converted to
        # radius.
        self.centlat=None
        self.centlon=None
        self.mindist=None # in km
        self.maxdist=None # in km
        # Waveforms time range retrieval
        self.mseed_starttime=None # UTC datetime
        self.mseed_endtime=None # UTC datetime
        # If set e.g."HL", it will retrieve data, only from that network
        # Can contain wildcards.
        self.network=None
        # If True (default), MiniSEED files with gaps and/or overlaps will be
        # rejected.
        self.reject_gaps=None
        # The minimum length of the data as a fraction of the requested time
        # frame. After a channel has been
        # downloaded it will be checked that its total length is at least
        # that fraction of the requested time span. Will be rejected otherwise.
        # Must be between 0.0 and 1.0, defaults to 0.9.
        self.minimum_length=None
        # Sanitize makes sure that each MiniSEED file also has an associated
        # StationXML file, otherwise the MiniSEED files will be deleted
        # afterwards. This is potentially not desirable for large noise data
        # sets.
        self.sanitize=None
        # The minimum inter-station distance in meters. Data from any new
        # station  closer to any existing station will not be downloaded. Also
        # used for duplicate station detection as sometimes stations have
        # different names for different webservice providers.
        # Defaults to 1000 m.
        self.mindist_interstation=None
        # List of the channels (with wildcards)
        self.channel=None
        # List of the locations (with wildcards)
        self.location=None


        ########################### private variables #########################

        # MiniSEED data will be downloaded in bulk chunks. This settings
        # limits the chunk size. A higher number means that less total download
        # requests will be sent, but each individual download request will be
        # larger.
        self.__download_chunk=None
        # The number of download threads launched per client.
        self.__threads_per_client=None
        # configuration xml tree
        self.__tree=None
        # path to configuration xml file
        self.__path=None


    def read(self, xml):
        """
        Reading configuration xml file
        """
        self.__path=xml
        self.__tree=ET.parse(self.__path)
        _root=self.__tree.getroot()

        # retrieve providers
        self.providers=[]
        _providers=_root.find('providers')
        for _provider in _providers.findall('url'):
            self.providers.append(str(_provider.text))

        ', '.join(map(lambda x: "'" + x + "'", self.providers))
        # retrieve download domain
        _domain=_root.find('domain')
        self.centlat=round(float(_domain.find('center_latitude').text),4)
        self.centlon=round(float(_domain.find('center_longitude').text),4)
        self.mindist=int(_domain.find('min_distance').text)
        self.maxdist=int(_domain.find('max_distance').text)

        # retrieve waveforms info
        _waveforms=_root.find('waveforms')
        self.mseed_starttime=UTCDateTime(_waveforms.find('start_time').text)
        self.mseed_endtime=UTCDateTime(_waveforms.find('end_time').text)
        self.network=str(_waveforms.find('network').text)
        if self.network=='None': self.network=None

        self.reject_gaps=bool(_waveforms.find('no_gaps').text)
        self.minimum_length=float(_waveforms.find('minimum_length').text)
        self.sanitize=bool(_waveforms.find('sanitize').text)
        self.mindist_interstation=int(_waveforms.find('mindistance_interstation').text)

        # retrieve channel
        self.channel=[]
        _channel=_root.find('waveforms').find('channel').text
        if _channel==None: _channel=''
        self.channel.append(_channel)

        # retrieve location
        self.location=[]
        _location=_root.find('waveforms').find('location').text
        if _location==None: _location=''
        self.location.append(_location)



    def write(self, xml):
        """
        Writing configuration xml file
        """
        self.__tree.write(xml)

    def echo(self):
        """
        Printing configuration parameters
        """
        print "Configuration file: '" + self.__path + "'"
        for _p in self.providers: print "Providers -- url: " + _p
        print "Domain -- center_latitude: " + str(self.centlat)
        print "Domain -- center_longitude: " + str(self.centlon)
        print "Domain -- min_distance (km): " + str(self.mindist)
        print "Domain -- max_distance (km): " + str(self.maxdist)
        print "Waveforms -- start_time (UTC): " + str(self.mseed_starttime)
        print "Waveforms -- specific network: " + str(self.network)
        print "Waveforms -- reject channels with gaps: " + str(self.reject_gaps)
        print "Waveforms -- minimum_length (<=1.0): " + str(self.minimum_length)
        print "Waveforms -- only with station xml (sanitize): " + str(self.sanitize)
        print "Waveforms -- min_distance interstation (m): " + str(self.mindist_interstation)
        print "Waveforms -- channel: " + str(self.channel[0])
        print "Waveforms -- location: " + str(self.location[0])





if __name__ == "__main__":

    config=Configuration()
    config.read('configuration.xml')

    # print configuration to stdout
    config.echo()




