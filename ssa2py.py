#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of Download.
#
#    Download is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    Download is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Download.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import getopt, sys
import process

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:s:o:", ["help", \
        "config=", "stage=", "output="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        print "use -h to open main menu"
        sys.exit(1)

    config_path=None
    stage="download"
    output_path=None

    for option, value in opts:
        if option in ("-h", "--help"):
            print "Download -- v 1.0\n"
            print "-c, --config= <value>:"
            print "\tpath to configuration xml file\n"
            print "-o, --output= <value>:"
            print "\tpath to output folder\n"
            print "-s, --stage= <value>:"
            print "\tvalue can be: 'download', 'correct', 'run'"
            print "\t'download': retrieve mini-seeds from fdsn services"
            print "\t'correct': correct mini-seeds and export stations.xml"
            print "\t'run': calculate the SSA algorithm and export results"
            sys.exit(0)

        elif option in ("-c", "--config"):
            config_path=value

        elif option in ("-s", "--stage"):
            if value in ("download", "correct", "run"):
                stage=value
            else:
                assert False, "stage's argument must be one of these: " + \
                "(download, correct, run)"
                sys.exit(1)

        elif option in ("-o", "--output"):
            output_path=value

        else:
            assert False, "unhandled option"

    if config_path==None or stage==None or output_path==None:
        assert False, "Both 'config', 'stage' and 'output' must be set"


################################## Main Code ##################################

    proc = process.Process()

    print "Stage: " + stage

    print "Loading configuration..."
    print "From: " + config_path
    proc.loadConfig(config_path)

    # print configuration to stdout
    proc.config.echo()

    print "Initializing save directory..."
    print "To: " + output_path
    proc.init_dir(output_path)

    # write configuration to log file
    proc.log.configuration(proc.config)

    print "Downloading data..."
    print "To: " + proc.mseeds_dir
    proc.log.info("Downloading data...")
    proc.log.info("To: "+ proc.mseeds_dir)
    proc.download()

    print "Loading stations..."
    print "From: " + proc.stations_dir
    proc.log.info("Loading stations...")
    proc.log.info("From: "+ proc.stations_dir)
    proc.loadStations()

    print "Plotting data..."
    print "To: " + proc.plots_dir
    proc.log.info("Plotting data...")
    proc.log.info("To: "+ proc.plots_dir)
    proc.plot_mseeds()

    print "Plotting map..."
    print "To: " + proc.stations_map
    proc.log.info("Plotting map...")
    proc.log.info("To: "+ proc.stations_map)
    proc.plot_map()


if __name__ == "__main__":
    main()
