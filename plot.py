#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python
import matplotlib.pyplot as plt
from obspy.geodetics.base import kilometer2degrees
from mpl_toolkits.basemap import Basemap


def plot_map(station_list, cent_lat, cent_lon, max_dist, plot_file):
    """
    Plotting a map with epicenter and possible stations according to distance
    and a map with with the beachball
    """

    # sets figure's dimensions
    _fig_x = 10
    _fig_y = 10
    fig = plt.figure(figsize=(_fig_x,_fig_y))

    # calculating map space
    _max = max_dist
    _max = int(round(_max * 1000 *2))
    _size = _max + int(round(_max/7.0))
    _diff = kilometer2degrees(round(_size/(2*2.0*1000)))

    parallels = [round(cent_lat,2), round((cent_lat-_diff),2),
                 round((cent_lat+_diff),2)]
    meridians = [round(cent_lon,2), round((cent_lon-_diff),2),
                 round((cent_lon+_diff),2)]

    m = Basemap(projection='laea', lat_0 = cent_lat,
                lon_0 = cent_lon, lat_ts=cent_lat,
                resolution = 'i', area_thresh = 0.1, width = _size,
                height = _size)
    m.drawparallels(parallels,labels=[1,0,0,0], color='grey', fontsize=10)
    m.drawmeridians(meridians,labels=[0,0,0,1], color='grey', fontsize=10)
    m.drawrivers(color='aqua')
    m.drawcoastlines(color='0.2')
    m.drawcountries(color='0.4')
    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua')
    x,y = m(cent_lon, cent_lat)

    # epicenter
    m.scatter(x, y, 150, color="#FF0000", marker="*", zorder=3,
              linewidths=1, edgecolor="k")

    # stations
    for station in station_list:
        x,y = m(station.longitude, station.latitude)
        m.scatter(x, y, 150, color="#33CC00", marker="^", zorder=3,
                  linewidths=1, edgecolor="k")
        plt.text(x+1800, y+3000, station.network+'.'+station.code, family="monospace",
                 fontsize=12)

    fig.savefig(plot_file)
