#Copyright (C) 2016  Triantafyllis Nikolaos
#
#This file is part of SSA2py.
#
#    SSA2py is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    SSA2py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with SSA2py.  If not, see <http://www.gnu.org/licenses/>.


#!/usr/bin/env python

import os
from obspy import read_inventory
from obspy import read

class Station:
    """
    The class used for defining station
    """
    def __init__(self):
        """
        Constructor of class
        """
        self.network=None # string
        self.code=None # string
        self.latitude=None # float
        self.longitude=None # float
        self.stream=None # obspy stream object
        self.xml=None # path to xml file
        self.mseeds=None # path to mini-seeds files (with wildcards)
        self.inv=None # obspy inventory object

    def __set(self):
        """
        Checks if is totally set
        """
        if (self is None or self.network is None or self.code is None or \
        self.stream is None or self.xml is None or self.inv is None):
            return False

        else:
            return True

    def __rotate(self, compo):
        """
        Rotate traces to ZNE orientation system, according to component
        (private function)
        """
        # get data, azimuth and dip for each Z, N and E orientation
        # and rotate them
        _data_Z, _data_N, _data_E = rotate2zne(
            self.stream.select(channel='*'+compo+'Z').traces[0].data,
            self.inv.select(channel='*'+compo+'Z')[0][0][0].azimuth,
            self.inv.select(channel='*'+compo+'Z')[0][0][0].dip,
            self.stream.select(channel='*'+compo+'N').traces[0].data,
            self.inv.select(channel='*'+compo+'N')[0][0][0].azimuth,
            self.inv.select(channel='*'+compo+'N')[0][0][0].dip,
            self.stream.select(channel='*'+compo+'E').traces[0].data,
            self.inv.select(channel='*'+compo+'E')[0][0][0].azimuth,
            self.inv.select(channel='*'+compo+'E')[0][0][0].dip
            )

        # store (in memory) new data to respective traces
        self.stream.select(channel='*'+compo+'Z').traces[0].data=_data_Z
        self.stream.select(channel='*'+compo+'N').traces[0].data=_data_N
        self.stream.select(channel='*'+compo+'E').traces[0].data=_data_E

    def echo(self):
        """
        Quick print of station's info
        """
        print "station: " + self.network + '.' + self.code
        print "xml: " + self.xml

    def load(self, station_xml, station_mseeds):
        """
        Loads station's info and mini-seeds to memory
        """
        if not self.__set():
            # load inventory
            self.inv=read_inventory(station_xml)

            # set station's quick info
            self.network=self.inv[0].code
            self.code=self.inv[0][0].code
            self.latitude=self.inv[0][0].latitude
            self.longitude=self.inv[0][0].longitude

            # load traces/waveforms
            self.stream=read(station_mseeds)
            # sort traces/waveforms
            self.stream.sort()
            # add meta data to streams
            self.stream.attach_response(self.inv)
            
            # set xml path
            self.xml=station_xml
            # set mini-seeds path (with wildcards)
            self.mseeds=station_mseeds

    def count(self):
        """
        Returns number of traces
        """
        if self.__set():
            return len(self.stream.traces)
        else:
            return 0

    def plot(self, png):
        """
        Plots stream (all traces) to png file
        """
        if self.__set():
            # get full directory of png path
            _plot_dir=os.path.dirname(png)

            # create plot directory if doesn't exist
            if not os.path.exists(_plot_dir):
                os.makedirs(_plot_dir)

            # plot all station's traces/waveforms to one plot
            self.stream.plot(outfile=png)

    def trim(self, starttime, endtime):
        """
        Trime to specific time-range (equal length to all traces)
        """
        if self.__set():
            _t1=UTCDateTime(starttime)
            _t2=UTCDateTime(endtime)

            self.stream.trim(_t1, _t2)

    def remove(self, channel):
        """
        Remove trace based on channel
        e.g. *E (removes all E component channels/traces)
        """
        if self.__set():
            for _trace in self.stream.select(channel=channel):
                self.stream.remove(_trace)

    def removeGaps(self):
        """
        Remove traces with gaps
        """
        if self.__set():
            _gaps = self.stream.get_gaps()

            for _gap in _gaps:
                _channel=_gap[3] # get channel code of "gapped" stream
                self.removeTrace(_channel)

    def removeClips(self, threshold=0.8):
        """
        Remove clipped traces
        """
        if self.__set():
            _max_value = threshold * (2**23)

            for _trace in self.stream.traces:
                # if clipped
                if _max_value <= _trace.max():
                    # remove it
                    self.stream.remove(_trace)

    def removeResponse(self, display="VEL", pre_filt = [0.001, 0.005, 10, 20]):
        """
        Remove instrument response
        """
        if self.__set():
            self.stream.remove_response(inventory=self.inv,
                                        pre_filt=pre_filt, output=display)

    def rotate(self):
        """
        Rotate traces to ZNE orientation system
        """
        if self.__set():
            # get data, azimuth and dip for each Z, N and E orientation
            # rotate them and store them (in memory)
            if (self.count==3):
                self.__rotate('') # broadband or accelerometers channels
            if (self.count == 6):
                self.__rotate('H') # broadband channels
                self.__rotate('N') # accelerometers channels
